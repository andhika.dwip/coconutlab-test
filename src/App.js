import React, { useState, useEffect } from 'react';
import ThemeContext from './contexts/ThemeContext';
import Header from './components/header';
import Content from './components/content';
import Button from './components/button';
import Side from './components/aside';
import './assets/css/style.css';

function App() {
  const [theme, setTheme] = useState('light');

  useEffect(() => {
    document.body.className = theme;
  }, [theme]);

  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
    <div className={`App ${theme}`}>
      <div className='container-fluid'>
        <Header />

        <div className='main--container'>
          <Side 
            top={
              { title: "2m+", 
                sub: "nisi ut aliquid" 
              }
            }
            bottom={
              { title: "100m+", 
                sub: "autem quibusdam" 
              }
            } 
            position="left"
          />

          <Content />
          
          <Side 
            top={
              { title: "1k+", 
                sub: "rerum facilis" 
              }
            }
            bottom={
              { title: "120k", 
                sub: "libero tempore" 
              }
            } 
            position="right"
          />
        </div>

        <div className='container d-flex justify-content-center'>
          <Button title="TOGGLE DARK MODE"/>
        </div>
      </div>
    </div>
    </ThemeContext.Provider>
  );
}

export default App;
