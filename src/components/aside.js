import React from 'react';

const SectionSide = ({content, theme, position}) => {
    return (
        <div className={`content--side ${position}`}>
            <h6>{content.title}</h6>
            <span>{content.sub}</span>
        </div>
    );
}

const Side = ({top, bottom, position}) => {

    return (
        <div className={`side ${position}`}>
            <SectionSide position={position} content={top} />
            <div className='line'></div>
            <SectionSide position={position} content={bottom} />
        </div>
    );
}

export default Side;
