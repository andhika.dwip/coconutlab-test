import React, { useContext } from 'react';
import ThemeContext from '../contexts/ThemeContext';

const Button = ({title}) => {
    const { theme, setTheme } = useContext(ThemeContext);

    return (
        <div className='position-relative'>
            <button onClick={() => setTheme(theme === 'light' ? 'dark' : 'light')} className='btn'>{title}</button>
        </div>
    );
}

export default Button;
