import React from 'react';

const Content = () => {
    return (
        <div className='content--main'>
            <h1 className='content--main__title'>Consectetur, <span>adipisci</span> <br/> velit, sed <span>quia non</span></h1>
        </div>
    );
}

export default Content;
