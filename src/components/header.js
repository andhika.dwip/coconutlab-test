import React from 'react';

const Header = () => {
    return (
        <header className='container'>
            <p className='header--title'>Sed ut perspiciatis unde omnis iste natus error sit voluptatem  <br/> accusantium doloremque laudantium, totam rem aperiam</p>
        </header>
    );
}

export default Header;
